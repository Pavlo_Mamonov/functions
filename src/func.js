const getSum = (str1, str2) => {
  if (typeof str1 !== 'string' || typeof str2 !== 'string') return false
  if (str1 === '') str1 = new Array(str2.length + 1).join('0');
  if (str2 === '') str2 = new Array(str1.length + 1).join('0');
  let maxLength = str1.length >= str2.length ? str1.length : str2.length
  let result = []
  for (let index = 0; index < maxLength; index++) {
    if (isNaN(str1[index]) || isNaN(str2[index])) return false
    result[index] = Number.parseInt(str1[index]) + Number.parseInt(str2[index])
  }
  return result.join('');
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let numberOfPosts = 0;
  let numberOfComments = 0;
  for (const post of listOfPosts) {
    if (post['author'] === authorName) numberOfPosts++
    if (post.hasOwnProperty('comments')) {
      for (const comment of post['comments']) {
        if (comment['author'] === authorName) numberOfComments++
      }
    }
  }
  return 'Post:' + `${numberOfPosts}` + ',comments:' + `${numberOfComments}`;
};

const tickets=(people)=> {
  let sum = 0
  for (const money of people) {
    if ((money - 25) > sum ) return 'NO'
    sum += 25
  }
  return 'YES';
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
